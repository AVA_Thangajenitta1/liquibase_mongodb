wget_installation()
{
    yum update -y
    yum install wget -y
}
java_installation()
{
    yum install java-11-openjdk-devel -y
}
liquibase_installation()
{
    wget https://github.com/liquibase/liquibase/releases/download/v4.8.0/liquibase-4.8.0.tar.gz
    tar -xvzf liquibase-4.8.0.tar.gz
    echo ""export PATH=/builds/AVASOFTWAREINC/rac_postgres:$PATH"" >> ~/.bashrc
    source ~/.bashrc
}
jdbc_installation()
{
    wget https://repo1.maven.org/maven2/org/mongodb/mongo-java-driver/3.9.1/mongo-java-driver-3.9.1-javadoc.jar
}
liquibase_extension_installation()
{
    wget https://github.com/liquibase/liquibase-mongodb/releases/download/liquibase-mongodb-4.8.0/liquibase-mongodb-4.8.0.jar
}
